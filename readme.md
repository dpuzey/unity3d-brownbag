#Unity3d brown-bag

This project is supporting material for a Unity3d brown-bag session led by Dan Puzey on 30 July 2014. It aims to provide a very brief overview of the functionality available in Unity3d (version 4.6).
