﻿using UnityEngine;
using System.Collections;

public class Destroyable : MonoBehaviour
{
    public void Destroy()
    {
        GameObject.Destroy(this.gameObject);
    }
}
